#include <iostream>
using namespace std;

unsigned long long factorialIterative(unsigned short n)
{
    unsigned long long result = 1;

    for (int i = 1; i <= n; i++)
    {
        result *= i;
    }

    return result;
}

unsigned long long factorialRecursive(unsigned short n)
{
    if (n == 0 || n == 1)
    {
        return 1;
    }
    else
    {
        return factorialRecursive(n - 1) * n;
    }
}

unsigned int sumN(unsigned int n)
{
    if (n == 0)
    {
        return 0;
    }
    else
    {
        return sumN(n - 1) + n;
    }
}

unsigned int multiply(unsigned int n, unsigned int s)
{
    if (n == 0 || s == 0)
    {
        return 0;
    }
    else
    {
        return multiply(n - 1, s) + s;
    }
}

unsigned long long memoized[100]{ 0, 1 /* 0, 0, etc. */}; // all others intially zero

unsigned long long fibonacci(unsigned short n)
{
    if (n == 0)
    {
        return 0;
    }
    else if (memoized[n] != 0)
    {
        return memoized[n]; // already calculated
    }
    else
    {
        unsigned long long answer { fibonacci(n - 1) + fibonacci(n - 2) };
        memoized[n] = answer; // never calculate again
        return answer;
    }
}

unsigned long long fibonacciIterative(unsigned short n)
{
    unsigned long long last{ 1 };
    unsigned long long secondLast { 0 };

    for (unsigned short i = 2; i <= n; i++)
    {
        // calculate f(i)
        unsigned long long tmp = last + secondLast;

        // f(i - 2) becomes old f(i - 1)
        secondLast = last;

        // f(i - 1) becomes old f(i)
        last = tmp;
    }

    return last;
}

int main()
{
    for (unsigned int n{ 0 }; n < 100; n++)
    {
        cout << fibonacci(n) << endl;
    }

    return 0;
}